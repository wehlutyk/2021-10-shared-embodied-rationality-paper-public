#+TITLE: Presentation

* Presentation
** DONE Intro
I want to discuss the topic of rationality and control, versus flow, but at the sensorimotor level. I want to do that by doing a short review and presenting some work in progress in analyzing data from the Perceptual Crossing Experiment.

** DONE Why rationality and control
Why rationality and control?

Embodied, distributed, enactive cognitive science, but also anthropology have made it clear that to understand how people function we need to work at the level of the emerging system that we form together

One thing I really liked about Kaya de Barbaro's keynote on mother-infant relationships was the clarity of reciprocity and co-dependence that take place in mother-infant relationships. In fact it was so clear and obvious, she barely talked about it explicitly. I'm sure this was the case for all the other keynotes here too (but I haven't had the time to watch them yet).

But I think that outside our circles, this still needs much arguing for. Particularly if we want to start changing our intuitions about what it is to be human in a society, inside and outside academia. Inside academia, in order to empower ourselves to imagine better experiments. Outside academia, well you all know what thinking in terms of individuals is bringing our civilisation to.

The idea of partial acts as developed in Linguistic Bodies has done a lot of work here. Partial acts are when a person does X and can only succeed if another person responds appropriately. Giving a business card. You can look at this as consensus on what it is to behave appropriately given a situation. But you scale down and see it as consensus on how to couple with each other to be able to enact something together.

What I would like to start here is a discussion about the implications of these ideas inside competing frameworks. I mean frameworks that are routinely used in settings where nothing pushes us to think beyond the scales of individuals on one side and averaged system on the other. Where nothing is showing us how the situation is similar to mother-infant interaction.

And I'd like to explore the role of individual control in tasks, and how letting go of control, so to speak letting be, enables people to do more and better inside the frame of competing frameworks.

** Rationality -> utility -> game theory
Let's start with rationality. I'm going to make an ungodly hodgepodge of sources and theories here, but I want to give a feeling for the kind of framework that I think would be useful to get engaged in.

I start in 1952, when Leonard Savage publishes a mathematically tractable theory of Decision-Making which works in terms of individuals, preferences, rational decisions given knowledge about the world, and so on. He actually looks for a set of axioms on what preferences should look like, be ordered wrt each other and so on, and then derives that given those axioms there will be a unique utility function to represent preferences.

Savage does not make a clear definition of what rational is, but he had a huge influence models of rational decisions in economics.

Herbert Simon comes after this, and adds some subtleties by noticing that the axioms of Decision Theory are not really realistic, and looks at how they can be made more realistic. He notices that people rely on heuristics and aren't optimal in a given situation. Costs to gathering information, multivalued utility functions.

Daniel Kahneman then provided quite a taxonomy of other ways in which human beings don't behave according to vanilla Decision Theory, such as what heuristics look like, how risk-avert people are, or what the framing of a situation will change to decisions.

I want to mention here also 2 typical problems that are often used as thinking tools in what I consider to be competing frameworks: the exploration-exploitation problem (say a bee has found a nice patch of flowers, should it exploit this patch or go on exploring the rest of the area to see if other -- better -- patches can be found?), and the prisoner's dilemma (say 2 people are being accused of a crime, the authorities hold them separately, and ask them to testify against their partner. If both partners don't talk, they get out better than if they both talk, but if one talks and not the other things get even worse for the one who stays silent).

In terms of game theory, both partners defecting is called a Nash equilibrium, where neither can improve their situation just by changing their own strategy only. So these are the thinking tools that we often hear about when speaking of rationality, game theory, utility. And an interesting commonality is that in all these thinking tools, approaches, frameworks, it's always a matter of individuals making decisions, that can be game cooperative or not, and we always have individuals in control.

These don't look at the joint emergence of a decision, together.

** Intro PCE
I'd like to see what the PCE tells us inside this framework.

Intro PCE.

Very versatile:
- participants can co-regulate their interaction very freely
- experimenter can control

Also starting to be used in psychiatry, to study how people behave in real-time sensorimotor interactions and how patterns there are related to neurodiversity.

Before we go on, here are a few examples.

Later on you can see some kind of turn-taking that emerges.

In most studies after the original one in Auvray et al. 2009, this is framed as a cooperative game where each participant should help their partner find them. But people don't know how to do that. So in terms of individual rationality, maybe an optimal strategy would be to explore the space and click whenever we think we're touching the other avatar.

** Perceptual awareness through partial acts
Here is a case of optimal turn-taking.

Along with these behavioral measures, studies have asked participants to evaluate how much they felt the presence of the other person in each trial. On a Perceptual Awareness Scale, PAS.
1 - no experience of the other
2 - ambiguous experience
3 - almost clear
4 - clear

Now perceptual awareness is correlated with a measure of turn-taking. Turn-taking is measured as a single number for a trial, by looking at moments when one player is moving and the other is not, and computing how those moments alternate between participants in a trial. If one participant is moving the whole time and the other not at all, TT is 0. If one moves half the time while the other stays put, and then this swaps, then TT is maximal. So with more turn-taking, people become more aware of the presence of each other.

High perceptual awareness is also associated with people having more joint success in their clicks. In other words, having a strong awareness of the other is indicative of a process in which both participants have entered. When my partner doesn't find me (single success), I will have lower PAS of them. It turns out my awareness of you depends on you as much as it depends on me. And your participating in my awareness of you will depend on my engaging with you.

** Timing for active and passive touch
More recent studies have tried to quantify that.

One first point is that people will often click on each other nearly at the same time. Actually a short inter-click interval correlates with joint success in the task. This indicates that perceiving each other is something that is done together, not individually. It's harder to perceive the my partner if they are not perceiving me also, or available for perceiving me.

A second point is that successful perception of each other seems associated with a longer time lag in turn-taking. In this plot you see the maximum Windowed Cross Lagged Regression depending on PAS. It shows the time lag for which the movements of A and B resemble each other the most. So it indicates that for PAS 4, people take more time before reciprocating a stimulation, than for PAS 3.


The last interesting finding in the literature on this is the way information flows from one person to the other before and after a click. Here you see measures of transfer entropy (which is a flow of information) between a person's movement and the stimulation they receive. The bars here quantify how much of the stimulation that I receive is due to my movements (yellow bars) versus my partner's movements (green bars). 5 seconds before I click, the stimulation I receive comes from both partner and me, independent from PAS. 1s before a click, in trials of high PAS, the stimulation I receive comes from my partner: this is passive touch. After I click, the stimulation that I receive is due more to my movements than to my partner's. I am in active touch.

In other words: I click on you after you explore me. Of course, and you will explore me if you think it could me you're touching. And that will be the case if I explored you.

** Partial acts of oscillations
The last work I'd like to share is very much WIP, looks at the role of frequency matching in turn-taking.

Partial acts are about: I initiate something, and the shared act is successful if you answer back something that corresponds to what I did. In this case, the most salient aspect of partial acts in turn-taking is the frequency of the stimulation.

So in this trial, we look for instance at this time bin, and regress out any linear movements. So we get the movement of A, and B in that time bin, and look at the frequencies at which they oscillate, the spectrum. And we plot that over time. Here x is time, and y is the spectrum of A's movements in a 3 second window. So here before A clicks they are inactive and B is active, and the opposite before B clicks.

And we want to see if the actual frequency is part of the shared act: are people matching each other's frequencies in trials with higher awareness, or not.

So before a click, we take out 4 bins of 3 seconds, and 4 bins after a click. And we look at the correlation between the frequencies of A and B, for bins that follow each other. Say for PAS 2, we look at the frequency of A 4 bins before click, and does it match the frequency of B 3 bins before click (the bin that follows what happened on x). And as we look at higher PAS, there seems to be trends coming out, where A and B tend to match frequencies one after the other, a bit more after a click.

Again, this is work in progress and nothing here has been tested for significance, but it looks promising.

** Wrap-up
So let's wrap up. If you test for the dependencies between most of these properties, you see that PAS is linked to trial number, and to Joint recognition, which is linked to inter-click delay, which is linked to individual recognition.

What I've tried to show here is that finding each other in PCE is a shared act: I feel you if you explore me, and you explore me if you think it might be me, which is more likely if I explored you. So feeling each other is accomplished together. It's harder to do on my own (and of course it's impossible if my partner doesn't want to be found, so stays still).

Coming back to decision theory and game theory, what we have here is not so much that people behave as rational individuals or irrational individuals, it's that people are not in control of their own decision possibilities. To communicate, to interact, to reach sensorimotor coupling, each partner depends on the other. So nobody is individually in control of their exploration of the space. If they were, they wouldn't engage with the other when they found them.

You can't make a good decision (click or not click) without depending on the other, without coupling with the other; and that relies on you engaging with the other, and they engaging with you. One way to see this is that people are moving around, and throwing out offers for coupling. Whenever something answers, or when they receive such an offer, they explore locally and try answering back.

So let's come back to Savage, Simon, and Kahneman. In this line of thought, people started looking at norms of rationality that could lead to workable mathematics of individual decision, then looking at the ways in which people didn't follow those rationality norms. I think what's important here is not so much whether people behave according to individual-level rationality norms or not, it's that individual-level rationality is not what decision, games, or interactions are about. What the PCE shows is that it's not possible to make the best decision when maintaining complete control. What does enable a good decision is sharing control, which has nothing to do with individual-level decisions. It's engaging with the other, having a shared rationality with the other, to let a decision emerge in whatever it is that we enact together.
